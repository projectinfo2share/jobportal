package com.jp.validate;

import java.util.StringTokenizer;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.web.multipart.MultipartFile;

public class FileValidate implements ConstraintValidator<IFile, MultipartFile>{

	@Override
	public void initialize(IFile arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(MultipartFile file, ConstraintValidatorContext arg1) {
		
		String fileName=file.getOriginalFilename();
		String fName="",ext="";
		StringTokenizer st=new StringTokenizer(fileName,".");
		while(st.hasMoreTokens())
		{
			fName=st.nextToken();
			ext=st.nextToken();
		}
		
		if(file.isEmpty() || (ext.equalsIgnoreCase("doc") || ext.equalsIgnoreCase("docx")))
		{
		return false;
		}
		return true;
	}

}
