package com.jp.validate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Documented
@Constraint(validatedBy = OtherValidator.class)
public @interface Other {
	
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
