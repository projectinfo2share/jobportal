package com.jp.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class FresherRegDTO {

	private String email;
	private String password;
	private String cpassword;
	private String mobile;
	private int city;
	private String fname;
	private String lname;
	private String qualification;
	private int branch;
	private int university;
	private int college;

	private int gender;
	private float marks;
	MultipartFile file;
	private Date startDate;
	private Date endDate;
	private int state_id;
	private int country_id;
	private int nationality;

	public int getState_id() {
		return state_id;
	}

	public void setState_id(int state_id) {
		this.state_id = state_id;
	}

	public int getCountry_id() {
		return country_id;
	}

	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}

	public int getNationality() {
		return nationality;
	}

	public void setNationality(int nationality) {
		this.nationality = nationality;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCpassword() {
		return cpassword;
	}

	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getCity() {
		return city;
	}

	public void setCity(int city) {
		this.city = city;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch(int branch) {
		this.branch = branch;
	}

	public int getUniversity() {
		return university;
	}

	public void setUniversity(int university) {
		this.university = university;
	}

	public int getCollege() {
		return college;
	}

	public void setCollege(int college) {
		this.college = college;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public float getMarks() {
		return marks;
	}

	public void setMarks(float marks) {
		this.marks = marks;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "FresherRegDTO [email=" + email + ", password=" + password
				+ ", cpassword=" + cpassword + ", mobile=" + mobile + ", city="
				+ city + ", fname=" + fname + ", lname=" + lname
				+ ", qualification=" + qualification + ", branch=" + branch
				+ ", university=" + university + ", college=" + college
				+ ", gender=" + gender + ", marks=" + marks + ", file=" + file
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", state_id=" + state_id + ", country_id=" + country_id
				+ ", nationality=" + nationality + "]";
	}

}
