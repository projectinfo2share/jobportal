package com.jp.command;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;
import org.springframework.web.multipart.MultipartFile;

import com.jp.validate.IFile;
import com.jp.validate.Other;
import com.jp.validate.PasswordsEqualConstraint;
import com.jp.validate.Phone;
import com.jp.validate.Select;

@PasswordsEqualConstraint(message = "Password and Confirm passwords are not equal")
public class FresherRegCommand {

	@NotEmpty
	private String fname;
	@NotEmpty
	private String lname;
	@Email
	@NotEmpty
	private String email;
	@NotEmpty
	@Size(max = 8, min = 5)
	private String password;
	@NotEmpty
	@Size(max = 8, min = 5)
	private String cpassword;
	@NotEmpty
	@Phone
	private String mobile;

	@NotNull
	@Select(message = "Please select the gender")
	private int gender;
	@NotNull
	@Select(message = "Please select the Nationality")
	private int nationality;
	@NotNull
	@Select(message = "Please select the Country")
	private int country;
	@NotNull
	@Select(message = "Please select the State")
	private int state;
	@NotNull
	@Select(message = "Please select the City")
	private int city;
	@NotEmpty
	private String qualification;
	@NotNull
	@Select(message = "Please select the University")
	private int university;
	@NotNull
	@Select(message = "Please select the College")
	private int college;
	@NotNull
	@Select(message = "Please select the Branch")
	private int branch;
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date startDate;
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date endDate;
	@NotNull
	// @NumberFormat(style = Style.PERCENT)
	@Min(value = 0)
	@Max(value = 100)
	private float marks;

	// @IFile(message="Please Select The File/valid Extension (doc/docx)")
	private MultipartFile file;


	private String Uother;
	private String Cother;
	private String other;

	public String getUother() {
		return Uother;
	}

	public void setUother(String uother) {
		Uother = uother;
	}

	public String getCother() {
		return Cother;
	}

	public void setCother(String cother) {
		Cother = cother;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public int getCountry() {
		return country;
	}

	public void setCountry(int country) {
		this.country = country;
	}

	public int getNationality() {
		return nationality;
	}

	public void setNationality(int nationality) {
		this.nationality = nationality;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCpassword() {
		return cpassword;
	}

	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getCity() {
		return city;
	}

	public void setCity(int city) {
		this.city = city;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch(int branch) {
		this.branch = branch;
	}

	public int getUniversity() {
		return university;
	}

	public void setUniversity(int university) {
		this.university = university;
	}

	public int getCollege() {
		return college;
	}

	public void setCollege(int college) {
		this.college = college;
	}

	public float getMarks() {
		return marks;
	}

	public void setMarks(float marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "FresherRegCommand [email=" + email + ", password=" + password
				+ ", cpassword=" + cpassword + ", mobile=" + mobile + ", city="
				+ city + ", fname=" + fname + ", lname=" + lname
				+ ", qualification=" + qualification + ", branch=" + branch
				+ ", university=" + university + ", college=" + college
				+ ", gender=" + gender + ", marks=" + marks + ", startDate="
				+ startDate + ", endDate=" + endDate + "]";
	}

}
