package com.jp.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

import com.jp.bo.CityBO;
import com.jp.bo.StateBO;
import com.jp.dao.GetStateCityDao;

@Component("GetCityServlet")
public class GetCityServlet implements HttpRequestHandler {
static final Logger logger=Logger.getLogger(GetCityServlet.class);
	@Autowired
	GetStateCityDao getScity;

	@Override
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		if(logger.isDebugEnabled()){
			logger.info("GetCityServlet handleRequest");
		}
		String country = request.getParameter("countryname");
		String state = request.getParameter("stateName");
		StringBuffer sb = new StringBuffer("");
		int id=0;
		sb.append("<option value="+id+">Select</option>");

		List<CityBO> stList = new ArrayList<CityBO>();
		stList = getScity.getCities(Integer.parseInt(country),
				Integer.parseInt(state));

		for (int i = 0; i < stList.size(); i++) {
			CityBO stateBO = stList.get(i);
			sb.append("<option value=" + stateBO.getId() + ">");
			sb.append(stateBO.getName() + "</option>");
		}

		response.getWriter().println(sb.toString());

	}

}
