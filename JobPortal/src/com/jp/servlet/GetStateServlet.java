package com.jp.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

import com.jp.bo.StateBO;
import com.jp.dao.GetStateCityDao;

@Component("GetStateServlet")
public class GetStateServlet implements HttpRequestHandler {

	@Autowired
	GetStateCityDao getScity;

	@Override
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String country = request.getParameter("countryname");
		StringBuffer sb = new StringBuffer("");
		sb.append("<option value='0'>Select</option>");

		List<StateBO> stList = new ArrayList<StateBO>();
		stList = getScity.getStates(Integer.parseInt(country));

		for (int i = 0; i < stList.size(); i++) {
			StateBO stateBO = stList.get(i);
			sb.append("<option value='" + stateBO.getId() + "'>");
			sb.append(stateBO.getName() + "</option>");
		}

		response.getWriter().println(sb.toString());

	}

}
