package com.jp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;

import com.jp.dao.GetEmailAddressDAO;

@Component("CheckEmailServlet")
public class CheckEmailServlet implements HttpRequestHandler {

	@Autowired
	GetEmailAddressDAO emailDao;

	@Override
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("email");
		int emailAddrCnt = emailDao.findeEmail(email);
		PrintWriter out = response.getWriter();

		if (emailAddrCnt > 0) {
			out.println("<font color=red>");
			out.println("Email Already Exists");
			out.println("</font>");
		} else {
			out.println("<font color=green>");
			out.println("Available");
			out.println("</font>");
		}

	}
}
