package com.jp.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.jp.bo.BranchBO;
import com.jp.bo.CollegeBO;
import com.jp.bo.CountryBO;
import com.jp.bo.NationalityBO;
import com.jp.bo.UniversityBO;
import com.jp.command.FresherRegCommand;
import com.jp.dao.GetRegistrationDetailsDAO;
import com.jp.deligate.RegisterStudentDeligate;
import com.jp.dto.FresherRegDTO;

@Controller
public class FresherRegController {

	static final Logger logger=Logger.getLogger(FresherRegController.class);	
	
	
	@Autowired
	private GetRegistrationDetailsDAO getRegistrationDetailsDAO;
	@Autowired
	private FresherRegDTO fDto;
	@Autowired
	private RegisterStudentDeligate registerStudentDeligate;

	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("dd/MM/yyyy"), true, 10));
		binder.registerCustomEditor(Float.class, new CustomNumberEditor(
				Float.class, true));
	//	binder.findCustomEditor(I, propertyPath);		
	}

	
	@RequestMapping(method = RequestMethod.GET, value = "/freSelectForm.htm")
	public String renderExperienceForm(ModelMap model) {

		FresherRegCommand fCommand = null;
		fCommand = new FresherRegCommand();
		model.addAttribute("freCommand", fCommand);
		return "fresher";

	}

	@RequestMapping(method = RequestMethod.POST)
	public String rgisterFresher(ModelMap model,
			@Valid @ModelAttribute("freCommand") FresherRegCommand fCommand,
			BindingResult result) {

		if (result.hasErrors()) {
		return "fresher";
		}
if(logger.isDebugEnabled()){
	logger.debug("::debug mesage::");
}
if(logger.isDebugEnabled()){
	logger.info("::info message::");
}
if(logger.isDebugEnabled()){
	logger.error("::error message::");
}

if(logger.isDebugEnabled()){
	logger.warn("::warn message::");
}
		fDto.setEmail(fCommand.getEmail());
		fDto.setBranch(fCommand.getBranch());
		fDto.setCity(fCommand.getCity());
		fDto.setCollege(fCommand.getCollege());
		fDto.setCpassword(fCommand.getCpassword());
		fDto.setEndDate(fCommand.getEndDate());
		fDto.setFname(fCommand.getFname());
		fDto.setGender(fCommand.getGender());
		fDto.setLname(fCommand.getLname());
		fDto.setMarks(fCommand.getMarks());
		fDto.setMobile(fCommand.getMobile());
		fDto.setPassword(fCommand.getPassword());
		fDto.setQualification(fCommand.getQualification());
		fDto.setStartDate(fCommand.getStartDate());
		fDto.setUniversity(fCommand.getUniversity());
		fDto.setState_id(fCommand.getState());
		fDto.setCountry_id(fCommand.getCountry());
		fDto.setNationality(fCommand.getNationality());
		System.out.println(fDto);

		MultipartFile multipartFile = fCommand.getFile();
		System.out.println("multipart file :" + multipartFile.toString());
		String name = multipartFile.getOriginalFilename();
		
		System.out.println("size :"+multipartFile +"name "+name);

		if (!multipartFile.isEmpty()) {
			try {
				System.out.println("size :"+multipartFile.getSize());
				
				byte[] bytes = multipartFile.getBytes();
				
				

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");

				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File("D:/UploadedFiles" + File.separator
						+ name);

				System.out.println(dir.getAbsolutePath() + File.separator
						+ name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

			} catch (Exception e) {
				System.out.println("You failed to upload " + name + " => " + e.getMessage());
				return "fresher";
			}
		} else {
			System.out.println( "You failed to upload " + name
					+ " because the file was empty.");
			/*model.put("file", "You failed to upload " + name
					+ " because the file was empty.");*/
			return "fresher";
		}

		boolean res = registerStudentDeligate.registerFresher(fDto);
		if (res) {
			return "success";
		} else {
			return "failure";
		}

	}

	@ModelAttribute("branchDetails")
	public Map<Integer, String> getBranchDetails() {
		Map<Integer, String> branchMap = null;

		branchMap = new HashMap<Integer, String>();

		List<BranchBO> branchList = new ArrayList<BranchBO>();
		branchList = getRegistrationDetailsDAO.getBranches();
		branchMap.put(0, "Select");
		for (int i = 0; i < branchList.size(); i++) {
			BranchBO bo = branchList.get(i);
			branchMap.put(bo.getbID(), bo.getbName());
		}
		branchMap.put(-1, "Others");
		// System.out.println(branchMap);
		return branchMap;

	}

	@ModelAttribute("uniDetails")
	public Map<Integer, String> getUniversities() {
		Map<Integer, String> unhiversityMap = null;
		unhiversityMap = new HashMap<Integer, String>();

		List<UniversityBO> uniList = new ArrayList<UniversityBO>();
		uniList = getRegistrationDetailsDAO.getUniversities();
		unhiversityMap.put(0, "Select");
		for (int i = 0; i < uniList.size(); i++) {
			UniversityBO universityBO = uniList.get(i);
			unhiversityMap.put(universityBO.getId(), universityBO.getName());
		}
		unhiversityMap.put(-1, "Others");
		
		return unhiversityMap;
	}

	@ModelAttribute("colleges")
	public Map<Integer, String> getColleges() {
		Map<Integer, String> clgMap = null;
		clgMap = new HashMap<Integer, String>();

		List<CollegeBO> clgList = new ArrayList<CollegeBO>();
		clgList = getRegistrationDetailsDAO.getColleges();
		clgMap.put(0, "Select");
		for (int i = 0; i < clgList.size(); i++) {
			CollegeBO collegeBO = clgList.get(i);
			clgMap.put(collegeBO.getId(), collegeBO.getName());
		}
		
		clgMap.put(-1, "Others");
		return clgMap;

	}

	@ModelAttribute("nationalities")
	public Map<Integer, String> getNationalities() {
		Map<Integer, String> natMap = null;
		natMap = new HashMap<Integer, String>();

		List<NationalityBO> natList = new ArrayList<NationalityBO>();
		natMap.put(0, "Select");
		natList = getRegistrationDetailsDAO.getNtionalities();

		for (int i = 0; i < natList.size(); i++) {
			NationalityBO nationalityBO = natList.get(i);
			natMap.put(nationalityBO.getId(), nationalityBO.getName());
		}

		return natMap;
	}

	@ModelAttribute("countries")
	public Map<Integer, String> getCountries() {
		Map<Integer, String> catMap = null;
		catMap = new HashMap<Integer, String>();

		List<CountryBO> catList = new ArrayList<CountryBO>();
		catMap.put(0, "Select");
		catList = getRegistrationDetailsDAO.getCountries();

		for (int i = 0; i < catList.size(); i++) {
			CountryBO countryyBO = catList.get(i);
			catMap.put(countryyBO.getId(), countryyBO.getName());
		}

		return catMap;
	}

	@ModelAttribute("genderList")
	public Map<Integer, String> getGender() {
		Map<Integer, String> genMap = null;
		genMap = new HashMap<Integer, String>();

		genMap.put(0, "Select");
		genMap.put(1, "Male");
		genMap.put(2, "FeMale");

		return genMap;
	}

	/*
	 * @ModelAttribute("months") public Map<Integer, String> getMonths() {
	 * Map<Integer, String> monMap = null; monMap = new HashMap<Integer,
	 * String>();
	 * 
	 * List<MonthBo> mntList = new ArrayList<MonthBo>(); mntList =
	 * getRegistrationDetailsDAO.getMonths(); monMap.put(0, "Select"); for (int
	 * i = 0; i < mntList.size(); i++) { MonthBo monthBo = mntList.get(i);
	 * monMap.put(monthBo.getId(), monthBo.getName()); } return monMap; }
	 * 
	 * @ModelAttribute("years") public Map<Integer, String> getYears() {
	 * Map<Integer, String> yearMap = null; yearMap = new HashMap<Integer,
	 * String>();
	 * 
	 * Calendar calendar = Calendar.getInstance(); Date date =
	 * calendar.getTime();
	 * 
	 * int year = date.getYear(); yearMap.put(0, "Select"); for (int i = 3; i >=
	 * 0; i--) { String yearS = String.valueOf(((year + 1900) - i));
	 * yearMap.put(((year + 1900) - i), yearS); }
	 * 
	 * return yearMap;
	 * 
	 * }
	 */

}
