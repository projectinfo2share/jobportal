package com.jp.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegistrationController {
static final Logger logger=Logger.getLogger(RegistrationController.class);

	@RequestMapping(method = RequestMethod.GET, value = "/register.htm")
	
	public String renderRegisterForm() {
		return "registration";
	}

}
