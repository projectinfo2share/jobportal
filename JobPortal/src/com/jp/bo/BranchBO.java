package com.jp.bo;

public class BranchBO {
	private int bID;
	private String bName;

	
	public BranchBO(int bID, String bName) {
		this.bID = bID;
		this.bName = bName;
	}

	public int getbID() {
		return bID;
	}

	public void setbID(int bID) {
		this.bID = bID;
	}

	public String getbName() {
		return bName;
	}

	public void setbName(String bName) {
		this.bName = bName;
	}

}
