package com.jp.beans;

import org.springframework.beans.factory.annotation.Autowired;

import com.jp.dao.GetRegisterIdDAO;

public class GetStudentId {
	
	@Autowired
	private GetRegisterIdDAO getRegDao;
	public int getFresherId()
	{
		return getRegDao.getRegisterId();
	}

}
