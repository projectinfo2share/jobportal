package com.jp.dao;

import org.springframework.jdbc.core.JdbcTemplate;

public class GetRegisterIdDAO {
	
	
	private final String GET_ID="select count(1)+1 from personal_information";
	private final String GET_EDU_ID="select count(1)+1 from education_details";
	
	private JdbcTemplate jdbcTemplate;

	public GetRegisterIdDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int getRegisterId()
	{
		return jdbcTemplate.queryForInt(GET_ID);
	}
	
	public int getEduId()
	{
		return jdbcTemplate.queryForInt(GET_EDU_ID);
	}
	
	

}
