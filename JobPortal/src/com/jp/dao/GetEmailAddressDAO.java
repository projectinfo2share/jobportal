package com.jp.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


public class GetEmailAddressDAO {

	private final String GET_EMAIL_ADDRESS = "select count(1) from personal_information WHERE personal_information.EMAIL_ID=?";

	private JdbcTemplate jdbcTemplate;

	public GetEmailAddressDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int findeEmail(String id) {
		return jdbcTemplate.queryForObject(GET_EMAIL_ADDRESS, Integer.class,
				new Object[] { id });
	}

}
