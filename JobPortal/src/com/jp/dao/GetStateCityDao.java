package com.jp.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.jp.bo.CityBO;
import com.jp.bo.StateBO;

public class GetStateCityDao {

	private final String GET_STATE_DETAILS = "select id,state_name from state_master where country_id=?";
	private final String GET_CITY_DETAILS = "select id,city_name from city_master where country_id=? and state_id=?";

	private JdbcTemplate jdbcTemplate;

	public GetStateCityDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<StateBO> getStates(int countryId) {
		return jdbcTemplate.query(GET_STATE_DETAILS,
				new Object[] { countryId }, new StateRowMapper());
	}

	public List<CityBO> getCities(int country, int state) {
		return jdbcTemplate.query(GET_CITY_DETAILS, new Object[] { country,
				state }, new CityRowMapper());
	}

	private final class CityRowMapper implements RowMapper<CityBO> {

		@Override
		public CityBO mapRow(java.sql.ResultSet rs, int index)
				throws SQLException {
			return new CityBO(rs.getInt(1), rs.getString(2));
		}

	}

	private final class StateRowMapper implements RowMapper<StateBO> {

		@Override
		public StateBO mapRow(java.sql.ResultSet rs, int index)
				throws SQLException {
			return new StateBO(rs.getInt(1), rs.getString(2));
		}

	}

}
