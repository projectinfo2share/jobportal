package com.jp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;

import com.jp.bo.BranchBO;
import com.jp.bo.CollegeBO;
import com.jp.bo.CountryBO;
import com.jp.bo.MonthBo;
import com.jp.bo.NationalityBO;
import com.jp.bo.UniversityBO;
import com.mysql.jdbc.ResultSet;

public class GetRegistrationDetailsDAO {

	private final String GET_BRANCH_DETAILS = "SELECT ID,DESCRIPTION FROM BRANCH ORDER BY BRANCH.ID";
	private final String GET_UNIVERSITY_DETAILS = "SELECT ID,NAME FROM UNIVERSITY ORDER BY ID";
	private final String GET_COLLEGE_DETAILS = "SELECT ID,DESCRIPTION FROM COLLEGE ORDER BY ID";
	private final String GET_MONTH_DETAILS = "SELECT ID,NAME FROM MONTH_MASTER ORDER BY ID";
	private final String GET_NATIONALITY_DETAILS = "select id,nationality_name from nationality_master";
	private final String GET_COUNTRY_DETAILS = "select id,country_name from country_master";

	private JdbcTemplate jdbcTemplate;

	public GetRegistrationDetailsDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<BranchBO> getBranches() {
		List<BranchBO> branchList = null;
		branchList = jdbcTemplate.execute(new BranchStmtCreator(),
				new BranchStmtCallback());
		return branchList;
	}

	public List<UniversityBO> getUniversities() {
		List<UniversityBO> uniList = null;
		uniList = jdbcTemplate.execute(new UniStmtCreator(),
				new UniStmtCallback());
		return uniList;
	}

	public List<CollegeBO> getColleges() {
		List<CollegeBO> collList = null;
		collList = jdbcTemplate.execute(new CollegeStmtCreator(),
				new CollegeStmtCallback());
		return collList;
	}

	public List<NationalityBO> getNtionalities() {
		List<NationalityBO> natList = null;
		natList = jdbcTemplate.execute(new NationalityStmtCreator(),
				new NationalityStmtCallback());
		return natList;
	}

	public List<CountryBO> getCountries() {
		List<CountryBO> cntList = null;
		cntList = jdbcTemplate.execute(new CountryStmtCreator(),
				new CountryStmtCallback());
		return cntList;
	}

	private final class CountryStmtCreator implements PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement preparedStatement = null;
			preparedStatement = con.prepareStatement(GET_COUNTRY_DETAILS);
			return preparedStatement;
		}

	}

	private final class CountryStmtCallback implements
			PreparedStatementCallback<List<CountryBO>> {

		@Override
		public List<CountryBO> doInPreparedStatement(
				PreparedStatement pStatement) throws SQLException,
				DataAccessException {
			List<CountryBO> catList = null;
			ResultSet rs = (ResultSet) pStatement.executeQuery();
			catList = new ArrayList<CountryBO>();
			while (rs.next()) {
				catList.add(new CountryBO(rs.getInt(1), rs.getString(2)));
			}
			return catList;
		}

	}

	private final class NationalityStmtCreator implements
			PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement preparedStatement = null;
			preparedStatement = con.prepareStatement(GET_NATIONALITY_DETAILS);
			return preparedStatement;
		}

	}

	private final class NationalityStmtCallback implements
			PreparedStatementCallback<List<NationalityBO>> {

		@Override
		public List<NationalityBO> doInPreparedStatement(
				PreparedStatement pStatement) throws SQLException,
				DataAccessException {
			List<NationalityBO> natList = null;
			ResultSet rs = (ResultSet) pStatement.executeQuery();
			natList = new ArrayList<NationalityBO>();
			while (rs.next()) {
				natList.add(new NationalityBO(rs.getInt(1), rs.getString(2)));
			}
			return natList;
		}

	}

	private final class CollegeStmtCreator implements PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement statement = null;
			statement = con.prepareStatement(GET_COLLEGE_DETAILS);
			return statement;
		}

	}

	private final class CollegeStmtCallback implements
			PreparedStatementCallback<List<CollegeBO>> {

		@Override
		public List<CollegeBO> doInPreparedStatement(
				PreparedStatement pStatement) throws SQLException,
				DataAccessException {

			List<CollegeBO> clgList = null;
			ResultSet rs = null;
			clgList = new ArrayList<CollegeBO>();
			rs = (ResultSet) pStatement.executeQuery();
			while (rs.next()) {
				clgList.add(new CollegeBO(rs.getInt(1), rs.getString(2)));
			}
			return clgList;
		}

	}

	private final class UniStmtCreator implements PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement pStatement = null;
			pStatement = con.prepareStatement(GET_UNIVERSITY_DETAILS);
			return pStatement;
		}

	}

	private final class UniStmtCallback implements
			PreparedStatementCallback<List<UniversityBO>> {

		@Override
		public List<UniversityBO> doInPreparedStatement(
				PreparedStatement preparedStatement) throws SQLException,
				DataAccessException {
			List<UniversityBO> universityList = null;
			ResultSet rs = null;

			universityList = new ArrayList<UniversityBO>();
			rs = (ResultSet) preparedStatement.executeQuery();
			while (rs.next()) {
				universityList.add(new UniversityBO(rs.getInt(1), rs
						.getString(2)));
			}
			return universityList;
		}

	}

	private final class BranchStmtCreator implements PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement pStatement = null;
			pStatement = con.prepareStatement(GET_BRANCH_DETAILS);
			return pStatement;
		}

	}

	private final class BranchStmtCallback implements
			PreparedStatementCallback<List<BranchBO>> {

		@Override
		public List<BranchBO> doInPreparedStatement(PreparedStatement pStatement)
				throws SQLException, DataAccessException {
			List<BranchBO> branches = null;
			ResultSet rs = null;
			rs = (ResultSet) pStatement.executeQuery();
			branches = new ArrayList<BranchBO>();
			while (rs.next()) {
				branches.add(new BranchBO(rs.getInt(1), rs.getString(2)));
			}

			return branches;
		}

	}

}
