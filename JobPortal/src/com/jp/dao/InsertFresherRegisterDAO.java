package com.jp.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.jp.vo.EducationDetailsVO;
import com.jp.vo.PersonalInfoVO;

public class InsertFresherRegisterDAO {

	private final String INSERT_PERSONAL_INFO = "INSERT INTO personal_information (ID,FIRST_NAME,LAST_NAME,EMAIL_ID,MOBILE_NO,CITY,STATE,PROFILE_TYPE,GENDER,TITLE,country,nationality) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

	private final String INSERT_EDUCATION_DETAILS = "Insert into education_details (ID,LEVEL,START_DATE,END_DATE,BRANCH_ID,COLLEGE_ID,MARKS,USER_ID) VALUES (?,?,?,?,?,?,?,?)";

	private JdbcTemplate jdbcTemplate;

	public InsertFresherRegisterDAO(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int insertPersonalInfo(PersonalInfoVO pInfoVO) {
		return jdbcTemplate.update(
				INSERT_PERSONAL_INFO,
				new Object[] { pInfoVO.getId(), pInfoVO.getFname(),
						pInfoVO.getLname(), pInfoVO.getEmail(),
						pInfoVO.getMobile(), pInfoVO.getCity(),
						pInfoVO.getState(), 1, pInfoVO.getGender(), "",
						pInfoVO.getCountry(), pInfoVO.getNationality() });
	}

	public int insertEduDetails(EducationDetailsVO eDetailsVO) {
		return jdbcTemplate.update(INSERT_EDUCATION_DETAILS,
				new Object[] { eDetailsVO.getId(), 1,
						eDetailsVO.getStartDate(), eDetailsVO.getEndDate(),
						eDetailsVO.getBranch(), eDetailsVO.getCollege(),
						eDetailsVO.getMarks(), eDetailsVO.getUserId() });

	}

}
