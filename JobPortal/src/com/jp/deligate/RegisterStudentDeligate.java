package com.jp.deligate;

import org.springframework.beans.factory.annotation.Autowired;

import com.jp.dao.GetRegisterIdDAO;
import com.jp.dao.InsertFresherRegisterDAO;
import com.jp.dto.FresherRegDTO;
import com.jp.vo.EducationDetailsVO;
import com.jp.vo.PersonalInfoVO;

public class RegisterStudentDeligate {

	@Autowired
	private InsertFresherRegisterDAO fDao;
	@Autowired
	private GetRegisterIdDAO regDao;
	@Autowired
	private EducationDetailsVO eVo;
	@Autowired
	private PersonalInfoVO pVo;

	public boolean registerFresher(FresherRegDTO freDto) {

		int id = regDao.getRegisterId();
		int edu_id = regDao.getEduId();

		eVo.setId(edu_id);
		eVo.setBranch(freDto.getBranch());
		eVo.setCollege(freDto.getCollege());
		eVo.setEndDate(freDto.getEndDate());
		eVo.setMarks(freDto.getMarks());
		eVo.setQualification(freDto.getQualification());
		eVo.setUniversity(freDto.getUniversity());
		eVo.setStartDate(freDto.getStartDate());
		eVo.setUserId(id);

		pVo.setId(id);
		pVo.setCity(freDto.getCity());
		pVo.setCpassword(freDto.getCpassword());
		pVo.setEmail(freDto.getEmail());
		pVo.setFname(freDto.getFname());
		pVo.setLname(freDto.getLname());
		pVo.setGender(freDto.getGender());
		pVo.setMobile(freDto.getMobile());
		pVo.setPassword(freDto.getPassword());
		pVo.setState(freDto.getState_id());
		pVo.setCountry(freDto.getCountry_id());
		pVo.setNationality(freDto.getNationality());

		int j = fDao.insertPersonalInfo(pVo);
		int i = fDao.insertEduDetails(eVo);

		if (i > 0 && j > 0) {
			return true;
		}

		return false;
	}

}
