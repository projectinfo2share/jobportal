<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>



<meta http-equiv="X-UA-Compatible"
	content="IE=EmulateIE7; IE=EmulateIE9">
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" type="text/css" href="css/default.css" />
<script src="js/jquery-1.11.2.min.js"></script>


<title>Insert title here</title>
</head>

<script>
	$(document).ready(function() {

		if ($("#Usel option:selected").val() == -1) {
			$('#universityID').css('display', 'block');
			$('#Uother').css('display', 'block');
		}

		if ($("#sel option:selected").val() == -1) {
			$('#branchID').css('display', 'block');
			$('#other').css('display', 'block');
		}

		if ($("#Csel option:selected").val() == -1) {
			$('#collegeID').css('display', 'block');
			$('#Cother').css('display', 'block');
		}
	});
</script>


<script>
	$(function() {
		$('#sel').change(
				function() {
					$('#branchID').css('display',
							($(this).val() == -1) ? 'block' : 'none');
					$('#other').css('display',
							($(this).val() == -1) ? 'block' : 'none');

				});
	});

	$(function() {
		$('#Csel').change(
				function() {

					$('#collegeID').css('display',
							($(this).val() == -1) ? 'block' : 'none');
					$('#Cother').css('display',
							($(this).val() == -1) ? 'block' : 'none');

				});
	});
	$(function() {
		$('#Usel').change(
				function() {
					$('#universityID').css('display',
							($(this).val() == -1) ? 'block' : 'none');
					$('#Uother').css('display',
							($(this).val() == -1) ? 'block' : 'none');
				});
	});
</script>

<script type="text/javascript">
	function chkMarks(val) {

	}
</script>



<script type="text/javascript">
	function getXMLHttpRequestObject() {
		var xmlhttp;
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp = false;
			}
		}

		if (!xmlhttp) {
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				xmlhttp = false;
			}
		}
		return xmlhttp;
	}

	function getStates() {

		var http = new getXMLHttpRequestObject();
		var url = "getState?countryname="
				+ document.getElementById('countryId').value;
		http.open("GET", url, true);
		http.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		http.timeout = 1000;
		http.onreadystatechange = function() {
			if (http.readyState == 4) {
				if (http.status == 200) {
					document.getElementById("stateId").innerHTML = http.responseText;
				}
			}
		}
		http.send(null);
	}

	function getCities() {

		var http = new getXMLHttpRequestObject();
		var url = "getCity?countryname="
				+ document.getElementById('countryId').value + "&stateName="
				+ document.getElementById('stateId').value;
		http.open("GET", url, true);
		http.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		http.timeout = 1000;
		http.onreadystatechange = function() {
			if (http.readyState == 4) {
				if (http.status == 200) {
					document.getElementById("cityId").innerHTML = http.responseText;
				}
			}
		}
		http.send(null);
	}
</script>

<script type="text/javascript">
	function loadXMLDoc() {
		var xmlhttp;
		var k = document.getElementById("email").value;
		var urls = "checkMail?email=" + k;

		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				document.getElementById("err").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", urls, true);
		xmlhttp.send();
	}
</script>



<style>
.error {
	color: red;
	font-weight: bold;
}
</style>
</head>

<body>
	<center>
		<a href="freSelectForm.htm">Fresher</a> &nbsp;&nbsp; <a
			href="expSelectForm.htm">Experience</a>
	</center>




	<!-- <input type="radio" name="ptype" value="1" onChange="return selectPtype(this)"/>Fresher&nbsp;&nbsp;<input type="radio" name="ptype" value="2" onChange="return selectPtype(this)"/>Experience -->
	<br>

	<form:form commandName="freCommand" class="register"
		enctype="multipart/form-data">

		<br />
		<h1>Registration</h1>
		<fieldset class="row1">
			<legend>Login Details</legend>

			<p id="parentPermission">
				<label>First Name <font color="red">*</font> :
				</label>
				<form:input path="fname" tabindex="1" />
				<form:errors path="fname" class="error" />
				<label>Last Name <font color="red">*</font> :
				</label>
				<form:input path="lname" tabindex="2" />
				<form:errors path="lname" class="error" />
			</p>

			<p>
				<label>Email <font color="red">*</font> :
				</label>
				<form:input path="email" id="email" onkeyup="loadXMLDoc()"
					tabindex="3" />
				<span><div id="err"></div></span>
				<form:errors path="email" class="error" />
			</p>

			<p>
				<label>Password <font color="red">*</font> :
				</label>
				<form:password path="password" tabindex="4" />
				<form:errors path="password" class="error" />
			</p>

			<p>
				<label>Re-Enter Password <font color="red">*</font> :
				</label>
				<form:password path="cpassword" tabindex="5" />
				<form:errors path="cpassword" class="error" />
			</p>

		</fieldset>
		<fieldset class="row2">
			<legend>Personal Details </legend>

			<p>
				<label>Mobile <font color="red">*</font> :
				</label>
				<form:input path="mobile" maxlength="10" />
				<form:errors path="mobile" class="error" />
			</p>

			<p>
				<label>Gender <font color="red">*</font> :
				</label>
				<form:select path="gender" items="${genderList}">
					<%-- <form:option value="" selected>Select</form:option> --%>

				</form:select>
				<form:errors path="gender" class="error" />
			</p>

			<p>
				<label>Nationality <font color="red">*</font> :
				</label>
				<form:select path="nationality" items="${nationalities}">
					<%--  <form:option value="" selected>Select</form:option> --%>
				</form:select>
				<form:errors path="nationality" class="error" />
			</p>
			<p>
				<label>Country <font color="red">*</font> :
				</label>
				<form:select path="country" id="countryId" items="${countries}"
					onchange="return getStates()">
					<%-- <form:option value="" selected>Select</form:option> --%>
				</form:select>
				<form:errors path="country" class="error" />
			</p>
			<p>
				<label>State <font color="red">*</font> :
				</label> <select name="state" id="stateId" onchange="return getCities()">

				</select>
				<form:errors path="state" class="error" />
				<!--      </p>
               <p> -->
				<label>City <font color="red">*</font> :
				</label> <select name="city" id="cityId">
				</select>
				<form:errors path="city" class="error" />
			</p>
		</fieldset>

		</br>
		<fieldset class="row3">
			<legend>Educational Details </legend>
			<p>
				<label>Highest Qualification <font color="red">*</font> :
				</label>
				<form:input path="qualification" />
				<form:errors path="qualification" class="error" />
			</p>
			<p>
				<label>University <font color="red">*</font> :
				</label>
				<form:select id="Usel" path="university" items="${uniDetails}">
					<%-- <form:option value="" selected>Select</form:option> --%>
				</form:select>
				<form:errors path="university" class="error" style="display: none;" />
			</p>
			<p>
				<label style="display: none;" id="universityID">University :</label>
				<form:input id="Uother" style="display: none;" path="Uother" />
				<form:errors path="Uother" class="error" style="display: none;" />
			</p>
			<p>
				<label>College Name <font color="red">*</font> :
				</label>
				<form:select id="Csel" path="college" items="${colleges}">
					<%--  <form:option value="" selected>Select</form:option> --%>
				</form:select>
				<form:errors path="college" class="error" style="display: none;" />
			</p>
			<p>
				<label style="display: none;" id="collegeID">College Name :</label>
				<form:input id="Cother" path="Cother" style="display: none;" />
				<form:errors path="Cother" class="error" />
			</p>
			<p>
				<label>Branch <font color="red">*</font> :
				</label>
				<form:select id="sel" path="branch" items="${branchDetails}">
					<%-- <form:option value="" selected>Select</form:option> --%>
				</form:select>
				<form:errors path="branch" class="error" />
			</p>
			<p>
				<label style="display: none;" id="branchID">Branch :</label>
				<form:input id="other" path="other" style="display: none;" />
				<form:errors path="other" class="error" />
			</p>

			<p>
				<label>Start Date <font color="red">*</font> :
				</label>
				<form:input path="startDate" placeholder="dd/MM/yyyy" />
				<form:errors path="startDate" class="error" />
			</p>

			<p>
				<label>End Date <font color="red">*</font> :
				</label>
				<form:input path="endDate" placeholder="dd/MM/yyyy" />
				<form:errors path="endDate" class="error" />
			</p>

			<p>
				<label>Marks <font color="red">*</font> :
				</label> <input type="radio" name="marksType" value="Percentage" checked>&nbsp;&nbsp;<span>Percentage</span>
				<input type="radio" name="marksType" value="CGPA"
					style="margin-left: 13px;">&nbsp;&nbsp;<span>CGPA(out
					of 10)</span>
				<form:input class="form-control input-sm" path="marks" id="marks"
					style="width: 116px; display: inline; margin-left: 35px;"
					x-data-toggle-x="tooltip"
					title="Enter your marks in Percentage/CGPA (only the value)" />
				<form:errors path="marks" class="error" />
			</p>
			<p>
				<label>Upload Resume <font color="red">*</font> :
				</label>
				<form:input type="file" path="file" size="50" />
				<%-- <form:errors path="file" class="error"/> --%>
			</p>
		</fieldset>

		<div>
			<button class="button"">Register &raquo;</button>
		</div>

	</form:form>
</body>


</html>