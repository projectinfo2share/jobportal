<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<center>
<body>
      <form:form commandName="expCommand">
         	 <p style="color:red;">
				<form:errors path="*"/>
			</p>
			<br/>
			<table>
			  <tr>
			    <td>Email :</td>
			    <td><form:input path="email"/></td>
			  </tr>
			  <tr>
			    <td>Password :</td>
			    <td><form:input path="password"/></td>
			  </tr>
			   <tr>
			    <td>Re-enter Password :</td>
			    <td><form:input path="cpassword"/></td>
			  </tr>
			  <tr>
			    <td>Mobile Number :</td>
			    <td><form:input path="mobile"/></td>
			  </tr>
			  <tr>
			    <td>City :</td>
			    <td><form:input path="city"/></td>
			  </tr>	
			  <tr>
			    <td>First Name: :</td>
			    <td><form:input path="fname"/></td>
			  </tr>	
			  <tr>
			    <td>Last Name: :</td>
			    <td><form:input path="lname"/></td>
			  </tr>
			  <tr>
			    <td>Highest Qualification/Education :</td>
			    <td><form:input path="qualification"/></td>
			  </tr>
			  <tr>
			    <td>Highest Qualification/Education :</td>
			    <td>
			    <form:select path="qualification">
			      <form:option value="select">Select</form:option>
			    </form:select>
			    </td>
			  </tr>	
			  <tr>
			    <td>Branch :</td>
			    <td>
			    <form:select path="branch">
			      <form:option value="select">Select</form:option>
			    </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td>university :</td>
			    <td>
			    <form:select path="university">
			      <form:option value="select">Select</form:option>
			    </form:select>
			    </td>
			  </tr>	
			  <tr>
			    <td>College Name :</td>
			    <td>
			    <form:select path="college">
			      <form:option value="select">Select</form:option>
			    </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td>Passed out :</td>
			    <td>Month
			    <form:select path="month">
			      <form:option value="select">Select</form:option>
			    </form:select>
			    Year
			    <form:select path="year">
			      <form:option value="select">Select</form:option>
			    </form:select>			    
			    </td>
			  </tr>
			  <tr>
			    <td>Marks : <input type="radio" name="marksr">Percentage <input type="radio" name="marksr">CGPA(out of 10) </td>
			    <td>
                  <form:input path="marks"/>
			    </td>
			  </tr>			  				  	  			  			  				  			  			  			  		  			    		  
			</table>
			
      </form:form>
</body>
</center>
</html>